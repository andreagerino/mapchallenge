import * as moment from 'moment'
import * as React from 'react'
import { FormattedDate } from 'react-intl'
import { Popup } from 'react-leaflet'

import { IFeature } from 'services/features/types'

import { FieldTitle, PopupContent, PopupRow, PopupTitle } from './style'

interface IFeaturePopupProps {
  feature: IFeature
}

const FeaturePopup: React.SFC<IFeaturePopupProps> = ({ feature }) => {
  const { properties } = feature
  const { status, 'asset_numb': assetN, type, material, number_lan: lanes, update_dat: updated } = properties
  return (
      <Popup>
        <PopupTitle>Ramp details</PopupTitle>
        <PopupContent>
          <PopupRow>
            <FieldTitle>Asset</FieldTitle>
            <p>{assetN}</p>
          </PopupRow>
          <PopupRow>
            <FieldTitle>Status</FieldTitle>
            <p>{status}</p>
          </PopupRow>
          <PopupRow>
            <FieldTitle>Type</FieldTitle>
            <p>{type}</p>
          </PopupRow>
          <PopupRow>
            <FieldTitle>Material</FieldTitle>
            <p>{material}</p>
          </PopupRow>
          <PopupRow>
            <FieldTitle>Lanes</FieldTitle>
            <p>{lanes}</p>
          </PopupRow>
          <PopupRow>
            <FieldTitle>Last updated</FieldTitle>
            <p>
              <FormattedDate value={moment(updated).toDate()} />
            </p>
          </PopupRow>
        </PopupContent>
      </Popup>
  )
}

export default FeaturePopup
