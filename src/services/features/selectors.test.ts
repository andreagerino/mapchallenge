import { IState } from 'reducers'
import * as selectors from './selectors'
import { IFeature } from './types'

describe('features selectors', () => {
  it('should have a selector to get features', () => {
    const data: IFeature[] = []
    const state = {
      features: {
        data,
      }
    }

    expect(selectors.featuresSelector(state as IState)).toEqual(data)
  })

  it('should have a selector to get last updated', () => {
    const updated = {}
    const state = {
      features: {
        updated,
      }
    }

    expect(selectors.updatedSelector(state as IState)).toEqual(updated)
  })

  it('should have a selector to get bounds', () => {
    const bounds = {}
    const state = {
      features: {
        bounds,
      }
    }

    expect(selectors.boundsSelector(state as IState)).toEqual(bounds)
  })

  it('should have a selector to get filters', () => {
    const filters = {}
    const state = {
      features: {
        filters,
      }
    }

    expect(selectors.filtersSelector(state as IState)).toEqual(filters)
  })

  it('should have a selector to get coordinates from feature', () => {
    const coordinate = {}
    const coordinates = [[coordinate]]
    const feature = {
      geometry: {
        coordinates,
      }
    }

    expect(selectors.coordinatesSelector(feature as IFeature)).toEqual(coordinate)
  })

  it('should have a selector to get material from feature', () => {
    const material = ''
    const feature = {
      properties: {
        material,
      }
    }

    expect(selectors.materialSelector(feature as IFeature)).toEqual(material)
  })

  it('should have a selector to get area from feature', () => {
    const area = 0
    const feature = {
      properties: {
        'area_': area,
      }
    }

    expect(selectors.areaSelector(feature as IFeature)).toEqual(area)
  })

  it('should have a selector to get size from feature', () => {
    const size = ''
    const feature = {
      size,
    }

    expect(selectors.sizeSelector(feature as IFeature)).toEqual(size)
  })
})
