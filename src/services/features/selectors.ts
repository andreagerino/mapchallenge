import { latLngBounds, LatLngTuple } from 'leaflet'
import { createSelector } from 'reselect'

import { IState } from 'reducers'
import { IFeature } from './types'

export const featuresSelector = (state: IState) => state.features.data
export const updatedSelector = (state: IState) => state.features.updated
export const boundsSelector = (state: IState) => state.features.bounds
export const filtersSelector = (state: IState) => state.features.filters
export const coordinatesSelector = (feature: IFeature) => feature.geometry.coordinates[0][0]
export const materialSelector = (feature: IFeature) => feature.properties.material
export const areaSelector = (feature: IFeature) => feature.properties.area_
export const sizeSelector = (feature: IFeature) => feature.size

export const featureBoundsSelector = createSelector(
  [featuresSelector],
  features => {
    const coordinates: number[][] = []
    features.reduce((previous, f) => {
      coordinatesSelector(f).map(
        coordinate => previous.push([...coordinate].reverse())
      )
      return previous
    }, coordinates)

    if(coordinates.length === 0){
      return null
    }

    const bounds = latLngBounds(coordinates as LatLngTuple[])
    return bounds
  }
)

const getBin = (area: number) => {
  if(area < 50){
    return 'Small'
  }else if(area >= 50 && area < 200){
    return 'Medium'
  }else{
    return 'Large'
  }
}

export const featuresWithSizeCategorySelector = createSelector(
  [featuresSelector],
  features => (
    features.map(feature => ({
      ...feature,
      size: getBin(areaSelector(feature))
    }))
  )
)

export const filteredFeaturesSelector = createSelector(
  [featuresWithSizeCategorySelector, filtersSelector],
  (features, filters) => (
    features.filter(feature => (
        Object.keys(filters).reduce((previous, key) => {
            switch(key){
              case 'material':
                return previous && materialSelector(feature) === filters[key]
              case 'size':
                return previous && sizeSelector(feature) === filters[key]
              default:
                return previous
            }
        }, true)
      )
    )
  )
)

export const featuresWithinBoundsSelector = createSelector(
  [filteredFeaturesSelector, boundsSelector],
  (features, bounds) => {
    if(bounds == null){
      return features
    }

    return features.filter(f => {
      const coordinates = coordinatesSelector(f).map(
        coordinate => [...coordinate].reverse()
      )
      const contained = bounds.contains(coordinates[0] as LatLngTuple)

      return contained
    })
  },
)

export const materialCategorySelector = createSelector(
  [featuresWithinBoundsSelector],
  features => {
    if(features == null){
      return {}
    }

    const materials = features.reduce((previous, f) => {
      const material = materialSelector(f)
      let bin = previous[material]
      if(bin === undefined){
        bin = []
        previous[material] = bin
      }

      bin.push(f)

      return previous
    }, {})

    return materials
  }
)

export const sizeCategorySelector = createSelector(
  [featuresWithinBoundsSelector],
  features => {
    if(features == null){
      return {}
    }

    // Create bins [[0, 50), [50, 200), [200, 256)]
    const bins = features.reduce((previous, f) => {
      const binName = f.size
      let bin = previous[binName]
      if(bin === undefined){
        bin = []
        previous[binName] = bin
      }

      bin.push(f)
      return previous
    }, {})

    return bins
  }
)
