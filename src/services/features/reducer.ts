import { Bounds } from 'leaflet';
import * as Actions from './actions'
import { IFeature, IFilter } from './types'

export interface IFeaturesState {
  readonly bounds: Bounds | null,
  readonly data: IFeature[],
  readonly filters: IFilter,
  readonly updated: Date | null
}

export const featuresInit: IFeaturesState = {
  bounds: null,
  data: [],
  filters: {},
  updated: null
}

const features = (state = featuresInit, action: Actions.TActions) => {
  switch(action.type) {
    case Actions.FEATURES_SET:
      return {
        ...state,
        data: action.payload,
        updated: new Date(), 
      }
    case Actions.BOUNDS_SET:
      return {
        ...state,
        bounds: action.payload,
      }
    case Actions.FILTER_SET:
      return {
        ...state,
        filters: {
          ...state.filters,
          [action.meta]: action.payload,
        },
      }
    case Actions.FILTER_RESET:
      return {
        ...state,
        filters: {},
      }
    default:
      return state
  }
}

export default features
