export const MAP_URL = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}'
export const MAP_ID = 'mapbox.light'
export const MAP_TOKEN = 'pk.eyJ1IjoiYW5kcmVhZ2VyaW5vIiwiYSI6ImNqb3hwcHZ0ODIzeDQzcWxodXlzajI1ODQifQ.myrr0wzeKjwb7ZHTNlgEtA'
export const MAP_ATTRIBUTION = '© <a href="https://www.mapbox.com/about/maps/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> <strong><a href="https://www.mapbox.com/map-feedback/" target="_blank">Improve this map</a></strong>'
export const MAP_DEFAULT_ZOOM = 12
export const MAP_MAX_ZOOM = 19
