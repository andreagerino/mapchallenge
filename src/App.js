import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Spinner from 'components/spinner/'
import LastUpdated from 'components/lastUpdated/'
import FeatureMap from 'scenes/featureMap/'
import Stats from 'scenes/stats/'

import { getFeatures } from 'services/features/actions'
import { updatedSelector } from "services/features/selectors"

import 'App.css'

export class App extends Component {
  state = {
      loading: false,
  }

  componentDidMount() {
    const { getFeatures } = this.props
    this.setState({ loading: true }, () => {
        getFeatures().then(() => this.setState({ loading: false }))
    })
  }

  render() {
    const { lastupdated } = this.props
    const { loading } = this.state

    return (
      <div className="App">
        <FeatureMap />
        <Stats />
        <LastUpdated datetime={lastupdated}/>
        <Spinner enabled={loading} />
      </div>
    )
  }
}

App.propTypes = {
  getFeatures: PropTypes.func.isRequired,
  lastupdated: PropTypes.object,
}

const mapStateToProps = state => ({
    lastupdated: updatedSelector(state)
})

const mapDispatchToProps = ({ getFeatures })

export default connect(mapStateToProps, mapDispatchToProps)(App)
