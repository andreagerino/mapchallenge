import { IFeature } from './types';

export const API_GATEWAY_URL = 'http://localhost:3030'
export const FEATURES_URL = `${API_GATEWAY_URL}/features`

type FeatureArrayOrNull = IFeature[] | null
export const fetchFeatures: (() => Promise<FeatureArrayOrNull>) = () => (
  fetch(FEATURES_URL).then(response => {
    switch(response.status){
      case 200:
        return response.json()
      default:
        return null
    }
  }).catch(error => {
    return []
  })
)
