import * as React from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'

import StatGraph from './components/statGraph'

import { IState } from 'reducers'
import { materialCategorySelector, sizeCategorySelector } from 'services/features/selectors'

const StatsContainer = styled.div`
  display: flex;
  flex: 1 1 0;
  flex-direction: column;
  overflow: scroll;
  border-left: solid medium navy;

  @media screen and (max-width: 767px) {
    width: 100%;
    flex-direction: row;
    border-top: solid medium navy;
    border-left: none;
  }

`

const countElements = (object: object) => {
  const counts = {}
  Object.keys(object).map(key => {
    const elements = object[key]
    counts[key] = elements.reduce((previous: number) => previous + 1, 0)
    return counts[key]
  })

  return counts
}

interface IStatsProps {
  materials: string[],
  sizes: string[]
}

export const Stats: React.SFC<IStatsProps> = ({ materials, sizes }) => {
  const materialCounts = countElements(materials)
  const sizeCounts = countElements(sizes)

  return (
    <StatsContainer>
      <StatGraph title="material" data={materialCounts} />
      <StatGraph title="size" data={sizeCounts} />
    </StatsContainer>
  )
}

const mapStateToProps = (state: IState) => ({
  materials: materialCategorySelector(state),
  sizes: sizeCategorySelector(state),
})

export default connect(mapStateToProps)(Stats)
