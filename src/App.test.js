import React from 'react'
import sinon from 'sinon'
import { shallow } from 'enzyme'

import { App } from './App'

import Spinner from 'components/spinner/'
import FeatureMap from 'scenes/featureMap/'
import Stats from 'scenes/stats/'

const defaultProps = {
  features: [],
  getFeatures: () => {},
}

describe('<App>', () => {
  it('renders expected components and load features once mounted', () => {
    const getFeatures = sinon.stub().returns(Promise.resolve())
    const wrapper = shallow(<App {...defaultProps} getFeatures={getFeatures}/>)
    expect(getFeatures.called)
    expect(wrapper.find(Spinner).length).toEqual(1)
    expect(wrapper.find(FeatureMap).length).toEqual(1)
    expect(wrapper.find(Stats).length).toEqual(1)
  })

  it('should enable spinner when loading and disable it on completion', () => {
    const promise = Promise.resolve()
    const getFeatures = sinon.stub().returns(promise)
    const wrapper = shallow(<App {...defaultProps} getFeatures={getFeatures}/>)
    expect(wrapper.find(Spinner).prop('enabled')).toEqual(true)

    return promise.then(() => {
      expect(wrapper.find(Spinner).prop('enabled')).toEqual(false)
    })
  })
})
