import styled from 'styled-components'

export const PopupContainer = styled.div`
  margin-left: auto;
  margin-right: auto;
  position: relative;
  z-index: 999;
  text-align: right;

  p {
    margin: 10px;
    font-weight: bold;
    padding: 10px;
    background-color: white;
    display: inline-block;
    border: 2px solid rgba(0,0,0,0.2);
    border-radius: 4px;
    cursor: pointer;
  }
`
