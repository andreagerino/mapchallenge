import { LatLngTuple } from 'leaflet'

export interface IFeature {
    readonly id: string,
    readonly properties: {
        readonly status: string,
        readonly asset_numb: string,
        readonly type: string,
        readonly material: string,
        readonly number_lan: number,
        readonly update_dat: Date
        readonly area_: number,
    },
    readonly geometry: {
        coordinates: LatLngTuple[][][],
    },
    readonly size: string
  }

export interface IFilter {
    material?: string,
    size?: 'Small' | 'Medium' | 'Large'
}