import styled from 'styled-components'

export const PopupContent = styled.div`
  width: 300px;
  height: 200px;
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;
`

export const PopupRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;

  p {
    flex-grow: 1;
    text-align: right;
  }
`

export const PopupTitle = styled.div`
  font-weight: bold;
  font-size: large;
  border-bottom: solid maroon 1px;
  padding-bottom: 5px;
`

export const FieldTitle = styled.span`
  font-weight: bold;
`
