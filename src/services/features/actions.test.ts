import { Bounds } from 'leaflet'
import { AnyAction } from 'redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as sinon from 'sinon'

import * as actions from './actions'
import * as api from './api'
import { IFeature } from './types'

const middlewares = [thunk]
const mockStore = configureStore(middlewares)

describe('features actions', () => {
  it('should create an action to set features', () => {
    const features: IFeature[] = []

    expect(actions.setFeatures(features)).toEqual({
      payload: features,
      type: actions.FEATURES_SET,
    })
  })

  it('should create an action to set bounds', () => {
    const bounds: Bounds = new Bounds([])

    expect(actions.setBounds(bounds)).toEqual({
      payload: bounds,
      type: actions.BOUNDS_SET,
    })
  })

  it('should create an action to set filters', () => {
    const meta = ''
    const payload = ''

    expect(actions.setFilter(meta, payload)).toEqual({
      meta,
      payload,
      type: actions.FILTER_SET,
    })
  })

  it('should create an action to reset filters', () => {
    expect(actions.resetFilter()).toEqual({
      type: actions.FILTER_RESET,
    })
  })

  it('should create an action to get features', () => {
    const features: IFeature[] = []
    const store = mockStore([])
    const spy = sinon.stub(api, 'fetchFeatures').returns(Promise.resolve(features))

    return store.dispatch(actions.getFeatures() as unknown as AnyAction).then(() => { // TODO: Investigate how to properly type thunks
      expect(spy.called).toEqual(true)
      expect(store.getActions()).toEqual([ actions.setFeatures(features) ])
    })
  })
})
