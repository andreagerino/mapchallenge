import { GeoJsonObject } from 'geojson'
import { Bounds, LatLngBounds, LatLngTuple, LeafletEvent } from 'leaflet'
import * as React from 'react'
import { GeoJSON, Map, Marker, TileLayer } from 'react-leaflet'
import MarkerClusterGroup from 'react-leaflet-markercluster'
import { connect } from 'react-redux'
import { IState } from 'reducers'

import { setBounds, TSetBounds } from 'services/features/actions'
import { featureBoundsSelector, featuresWithinBoundsSelector } from 'services/features/selectors'
import { IFeature } from 'services/features/types'

import FeaturePopup from './components/featurePopup'
import FilterPopup from './components/filterPopup'
import { MAP_ATTRIBUTION, MAP_DEFAULT_ZOOM, MAP_ID,  MAP_MAX_ZOOM, MAP_TOKEN, MAP_URL } from './constants'


const buildFeatureMarker = (feature: IFeature) => {
  const { geometry, id } = feature
  const { coordinates } = geometry
  const center = [...coordinates[0][0][0]].reverse() as LatLngTuple

  return coordinates && (
    <Marker key={id} position={center}>
      <FeaturePopup feature={feature}/>
      </Marker>
    )
}

interface IFeatureMapProps {
  features: IFeature[],
  featureBounds: LatLngBounds,
  sBounds: (bounds: Bounds) => TSetBounds,
}

export class FeatureMap extends React.Component<IFeatureMapProps> {
  private mapRef: React.RefObject<Map> = React.createRef()
 
  constructor(props: IFeatureMapProps) {
    super(props);
    this.updateBounds = this.updateBounds.bind(this)
  }

  public render() {
    const { features, featureBounds } = this.props
    let zoom = MAP_DEFAULT_ZOOM
    if(featureBounds){
      const { current: map } = this.mapRef
      if(map){
        zoom = map.leafletElement.getBoundsZoom(featureBounds)
      }
    }

    return (
      <Map
        style={{ height: '100%', flexGrow: 3, flexBasis: 0 }}
        ref={this.mapRef}
        center={(featureBounds && featureBounds.getCenter())}
        zoom={zoom}
        maxZoom={MAP_MAX_ZOOM}
        onzoomend={this.updateBounds}
        onmoveend={this.updateBounds}>
          <TileLayer
            attribution={MAP_ATTRIBUTION}
            url={MAP_URL}
            id={MAP_ID}
            accessToken={MAP_TOKEN}
          />
          <GeoJSON key={JSON.stringify(features)} data={features as unknown as GeoJsonObject}/>
          <MarkerClusterGroup>
            {features.map(buildFeatureMarker)}
          </MarkerClusterGroup>
          <FilterPopup />
        </Map>
   )
  }

  private updateBounds(event: LeafletEvent) {
    const { sBounds } = this.props
    const { target } = event

    const bounds: Bounds = target.getBounds()
    sBounds(bounds)
  }

}

const mapStateToProps = (state: IState) => ({
  featureBounds: featureBoundsSelector(state),
  features: featuresWithinBoundsSelector(state),  
})

const mapDispatchToProps = ({ sBounds: setBounds })

export default connect(mapStateToProps, mapDispatchToProps)(FeatureMap)
