import { combineReducers } from 'redux'
import features, { IFeaturesState } from "./services/features/reducer"

export interface IState {
  features: IFeaturesState
}

const app = combineReducers({
  features,
})

export default app
