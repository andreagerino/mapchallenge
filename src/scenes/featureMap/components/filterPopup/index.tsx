import * as React from 'react'
import { connect } from 'react-redux'

import { resetFilter, TResetFilter } from 'services/features/actions'
import { filtersSelector } from 'services/features/selectors'

import { IState } from 'reducers'
import { PopupContainer } from './style'

interface IFilterPopupProps {
  filters: object,
  rFilter: () => TResetFilter,
}

const FilterPopup: React.SFC<IFilterPopupProps> = ({ filters, rFilter }) => {
  if (Object.keys(filters).length !== 0) {
      return (
        <PopupContainer>
          <p onClick={rFilter}>Reset filters</p>
          </PopupContainer>
      )
    }

    return null
}

const mapStateToProps = (state: IState) => ({
  filters: filtersSelector(state),
})

const mapDispatchToProps = ({ rFilter: resetFilter })

export default connect(mapStateToProps, mapDispatchToProps)(FilterPopup)
