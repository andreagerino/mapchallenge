import React from 'react'
import ReactDOM from 'react-dom'
import { IntlProvider } from 'react-intl'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import 'normalize.css'
import './index.css'

import App from './App'
import appReducer from './reducers'

const middleware = (
  // eslint-disable-next-line
  window.__REDUX_DEVTOOLS_EXTENSION__ &&
  // eslint-disable-next-line
  compose(applyMiddleware(thunk), window.__REDUX_DEVTOOLS_EXTENSION__()))
  || applyMiddleware(thunk)

const store = createStore(appReducer, middleware)

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider locale={navigator.language}>
      <App />
    </IntlProvider>
  </Provider>,
  document.getElementById('root')
)
