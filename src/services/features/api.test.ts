import * as fetchMock from 'fetch-mock'
import { AnyAction } from 'redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import { FEATURES_URL, fetchFeatures } from './api'

const middlewares = [thunk]
const mockStore = configureStore(middlewares)

describe('features api', () => {
  it("should fetch features from api", () => {
    const store = mockStore([])
    fetchMock.mock(FEATURES_URL, {})
    store.dispatch(fetchFeatures as unknown as AnyAction) // TODO: Investigate how to properly type thunks

    expect(fetchMock.called(FEATURES_URL)).toEqual(true)
    fetchMock.restore()
  })
})
