import { ChartLegendLabelItem, ChartOptions } from 'chart.js'
import * as React from 'react'
import { Pie } from 'react-chartjs-2'
import { connect } from 'react-redux'
import { withSize } from 'react-sizeme'
import styled from 'styled-components'

import { setFilter, TSetFilter } from 'services/features/actions'

const Container = styled.div`
  position: relative;
  flex: 1 1 0;
  padding: 20px;

  @media screen and (max-width: 767px) {
    padding: 0px;
  }
`

interface IStatGraphProps {
  size: {
    width: number,
    height: number
  },
  title: string,
  data: object,
  sFilter: (filter: string, value: string) => TSetFilter
}

export const StatGraph: React.SFC<IStatGraphProps>  = ({ title, data, size, sFilter }) => {
  const labels = Object.keys(data)
  const dataset = {
      backgroundColor: [
        'rgb(255, 99, 132)',
        'rgb(255, 159, 64)',
        'rgb(255, 205, 86)',
        'rgb(75, 192, 192)',
        'rgb(54, 162, 235)',
        'rgb(153, 102, 255)',
        'rgb(201, 203, 207)'
      ],
      data: Object.values(data),
  }

  const graphData = {
    datasets: [dataset],
    labels,
  }

  const graphOptions: ChartOptions = {
    legend: {
      onClick: (_: MouseEvent, item: ChartLegendLabelItem) => {
        if(item.text) {
          sFilter(title, item.text)
        }
      },
      position: 'bottom'
    },
    maintainAspectRatio: false,
    title: {
      display: true,
      fontSize: 20,
      text: title,
    }
  }

  return (
    <Container>
      <Pie key={size.width} data={graphData} options={graphOptions} width={size.width}/>
    </Container>
  )
}

const mapDispatchToProps = { sFilter: setFilter }

const connectedStatGraph = connect(undefined, mapDispatchToProps)(StatGraph)
export default withSize()(connectedStatGraph)
