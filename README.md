# VORTEXA - Technical challenge

![UI](sshot.png)

## Background

The goal is to build a React and Redux-based UI to explore geographical data. The interface should have the following features:

1. A map to be able to visualise all the boat ramps.
2. A data visualisation of your choice that displays the number or ramps per construction material.
3. A data visualisation of your choice that displays the number of ramps per size category (values of `area` in 3 different ranges: `[0, 50)`, `[50, 200)`, and `[200, 526)`).
4. Zooming in the map should filter the visualised data to include only those ramps which are currently visible in the viewport.
5. Clicking on a data point on a visualisation, should filter the ramps on the map to reflect the selected data.

## Data

The `data.json` file contains a cleaned [GeoJSON](http://geojson.org/) data set ready to be served by the embdedded [json-server](https://github.com/typicode/json-server).

## Project

I have developed the application using the following main libraries/frameworks:
- ReactJS
- Redux
- Reselect
- Styled Components
- React-intl
- Leaflet.js
- React-ChartJS-2
- Moment.js
- ESLint
- Jest
- Enzyme
- Sinon

In total, development took around a couple of days (I have been multitasking a lot in the meanwhile), including the time to get up to speed with [Leaflet.js](https://leafletjs.com/), which I never used before.

## Installation

The project uses `Yarn` as package manager and all its dependancies can be installed using this command:
```
yarn install
```

## Execution

The project can be started by using the command:
```
yarn start
```
this will start both `json-server` and `webpack`.

The project supports the following four commands:
```
yarn start // Start json-server and development server
yarn build // Build a production-ready version of the application
yarn test // Start the Jest test runner
yarn data // Start json-server to serve geographical data
```

## Instructions
Navigate the map to explore the available boat ramps. Clicking on a cluster will automatically zoom the map in its area, while clicking on a marker will show a popup displaying boat ramp details. Quantitative information about boat ramps can be found on the side panel. Two pie charts will show information about the material and size of the boat ramps currently displayed on the map. Clicking on a legend item (e.g. Concrete in Material) will filter the boat ramps displayed on the map. Filters can be removed by clicking on the `Reset filters` button.

## Typescript
The project has been migrated to Typescript. I have used [React & Redux in TypeScript - Static Typing Guide](https://github.com/piotrwitek/react-redux-typescript-guide) as a source for Typescript coding best practices in React.

## Improvements
- Extend test coverage
- Add error reporting (e.g. alert when fetch fails)
- Use a service worker to cache data
- Implement proper server to fetch data using geo-localised queries
