import * as React from 'react'

import './spinner.css'

interface IProps {
  enabled: boolean
}

export const Spinner: React.SFC<IProps> = ({ enabled }) => enabled ? (
  <div className="lds-container">
    <div className="lds-facebook"><div/><div/><div/></div>
  </div>
) : null

export default Spinner
