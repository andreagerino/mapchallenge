import { action, ActionType } from 'typesafe-actions'

import { Bounds } from 'leaflet'
import { fetchFeatures } from './api'
import { IFeature } from './types'

export const FEATURES_SET = "FEATURES/SET"
export const BOUNDS_SET = "FEATURES/BOUNDS"
export const FILTER_SET = "FEATURES/FILTER"
export const FILTER_RESET = "FEATURES/RESET_FILTER"

export const setFeatures = (features: IFeature[] | null) => action(FEATURES_SET, features)
type TSetFeatures = ActionType<typeof setFeatures>

export const setBounds = (bounds: Bounds) => action(BOUNDS_SET, bounds)
export type TSetBounds = ActionType<typeof setBounds>

export const setFilter = (filter: string, value: string) => action(FILTER_SET, value, filter)
export type TSetFilter = ActionType<typeof setFilter>

export const resetFilter = () => action(FILTER_RESET)
export type TResetFilter = ActionType<typeof resetFilter>

export const getFeatures = () => (dispatch: (action: TSetFeatures) => void) => (
  fetchFeatures().then(features =>
    dispatch(setFeatures(features))
  )
)
type TGetFeatures = ActionType<typeof getFeatures>

export type TActions = 
  | TSetFeatures
  | TSetBounds
  | TSetFilter
  | TResetFilter
  | TGetFeatures