import { Bounds } from 'leaflet'
import { Action } from 'redux'
import * as sinon from 'sinon'

import { resetFilter, setBounds, setFeatures, setFilter } from './actions'
import featuresReducer, { featuresInit } from './reducer'
import { IFeature } from './types'

describe('features reducer', () => {
  let clock: sinon.SinonFakeTimers

  beforeAll(() => {
       clock = sinon.useFakeTimers();
   })

   afterAll(() => {
       clock.restore();
   })

  it('should return default state', () => {
    expect(featuresReducer(undefined, {} as Action)).toEqual(featuresInit)
  })

  it('should handle the addFeatures action', () => {
    const features = [{}]

    const action = setFeatures(features as IFeature[])
    const state = featuresReducer(undefined, action)

    expect(state).toEqual({
      ...featuresInit,
      data: features,
      updated: new Date()
    })
  })

  it('should handle the addBounds action', () => {
    const bounds = {}

    const action = setBounds(bounds as Bounds)
    const state = featuresReducer(undefined, action)

    expect(state).toEqual({
      ...featuresInit,
      bounds,
    })
  })

  it('should handle the setFilter action', () => {
    const filter = { test: '' }

    const action = setFilter('test', '')
    const state = featuresReducer(undefined, action)

    expect(state).toEqual({
      ...featuresInit,
      filters: filter,
    })
  })

  it('should handle the resetFilter action', () => {
    const action = resetFilter()
    const state = featuresReducer(undefined, action)

    expect(state).toEqual({
      ...featuresInit,
    })
  })
})
