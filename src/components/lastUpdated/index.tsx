import * as React from 'react'
import { DateSource, FormattedRelative } from 'react-intl'
import styled from 'styled-components'

const Container = styled.div`
  position: absolute
  left: 0px;
  bottom: 0px;
  z-index: 999;
  margin: 10px;
  padding: 10px;
  font-size: x-small;
  font-weight: bold;
  background-color: white;
  border: 2px solid rgba(0,0,0,0.2);
  border-radius: 4px;

  @media screen and (max-width: 767px) {
    visibility: hidden;
  }
`

interface IProps {
  datetime: DateSource
}

export const LastUpdated: React.SFC<IProps> = ({ datetime }) => datetime ? (
  <Container>
    Last updated: <FormattedRelative value={datetime}/>
  </Container>
) : null

export default LastUpdated
